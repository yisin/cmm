package com.yisin.commun.client;

import com.yisin.commun.modal.DataPackage;
import com.yisin.commun.server.HandleHelper;

public interface ISocketEvent {
	
	public boolean filter(DataPackage dataPackage);

    public void onMessage(DataPackage dataPackage);

    public void onClose();

    public void onError(Throwable e);

    public void onOpen();
    
    interface _ISocketEvent {
        
    }

}
