package com.yisin.commun.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import com.alibaba.fastjson.JSONObject;
import com.yinsin.other.LogHelper;
import com.yinsin.utils.ByteUtils;
import com.yisin.commun.modal.Client;
import com.yisin.commun.modal.Data;
import com.yisin.commun.modal.DataPackage;
import com.yisin.commun.modal.HandlerMethod;
import com.yisin.commun.util.JSONUtils;

public class TcpServerThread extends Thread {
    private static final LogHelper logger = LogHelper.getLogger(TcpServerThread.class);
    
	private Socket socket;
	private Client client;
	private IMessageHandler cbi;
	private InputStream is;
	private boolean cut = true;
	
	private TcpServerThread(){}

	TcpServerThread(Client client, Socket ss, IMessageHandler cbi) {
		this.client = client;
		this.socket = ss;
		this.cbi = cbi;
	}

	public void run() {
		try {
			int i = -1;
			String text = null;
			if (!this.socket.isClosed()) {
				this.is = this.socket.getInputStream();
				byte[] byt = (byte[]) null;
				byte[] tempByt = null, msgByt = null;
				DataPackage pack = null;
				JSONObject json = null;
				while (this.cut) {
					byt = new byte[1024];
					i = this.is.read(byt);
					if (i != -1) {
						if (i == 1024) {
							if (tempByt == null) {
								tempByt = byt;
							} else {
								tempByt = ByteUtils.joinByteArray(tempByt, byt);
							}
						} else {
							if (tempByt == null) {
								tempByt = ByteUtils.getByte(byt, i);
							} else {
								tempByt = ByteUtils.joinByteArray(tempByt,
								        ByteUtils.getByte(byt, i));
							}
							msgByt = ByteUtils.copyByteArray(tempByt);
							try {
							    text = new String(msgByt);
							    
							    TcpPolicyThread.sendPolicyFile(this.socket, text);
							    
							    json = JSONObject.parseObject(text);
				            	Data msgData = JSONUtils.toJavaObject(json, Data.class);
				            	msgData.setValue(json.get("value"));
				                
				                pack = new DataPackage(msgData);
				                if(this.cbi.filter(client, pack)){
				                	callOnMessage(client, pack);
				                }
								//this.cbi.onMessage(client, pack);
							} catch (Exception e) {
							    logger.error("接收消息异常：" + client.getSid(), e);
							}
							tempByt = null;
						}
					} else {
						this.cbi.onClose(this.client);
						this.cut = false;
					}
				}
			}
		} catch (IOException e) {
			this.cbi.onClose(this.client);
			this.cut = false;
		}
	}
	
	private void callOnMessage(Client client, DataPackage pack){
    	String url = pack.getReq();
    	if(url != null){
    		url = url.replaceAll("/", ".");
    	}
    	HandlerMethod handMethod = HandleHelper.getHandlerMethod(url);
    	if(null != handMethod){
    		try {
				handMethod.getMethod().invoke(handMethod.getInstance(), new Object[]{
					client, pack				
				});
			} catch (Exception e) {
				this.cbi.onMessage(client, pack);
			}
    	} else {
    		this.cbi.onMessage(client, pack);
    	}
    }
	
	private class _TcpServerThread {
        
    }
}