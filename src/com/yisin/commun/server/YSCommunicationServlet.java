package com.yisin.commun.server;

import java.lang.reflect.Method;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.yinsin.other.LogHelper;
import com.yinsin.utils.CommonUtils;

public class YSCommunicationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final LogHelper logger = LogHelper.getLogger(YSCommunicationServlet.class);
    private String clientClassPath = "";
    private int port = 10000;
    private static String[] copyright = {
    	"",
    	"  ██    ██    ██   ████████   ██    ██      ████",
    	"   ██  ██          ██               ██     ██ ██",
    	"    ████      ██    ██        ██    ██    ██  ██",
    	"     ██       ██     ████     ██    ██   ██   ██",
    	"     ██       ██       ███    ██    ██  ██    ██",
    	"     ██       ██         ██   ██    ██ ██     ██",
    	"     ██       ██   ████████   ██    ████      ██",
    	"",
		"※※※※※※※※※※※※※※※※※※※※※※※※※※※",
		"  Welcome Using Yisin Communication",
		"  Author  [Yisin]  QQ [214175590]",
		"  Website [http://www.yinsin.net]",
		"※※※※※※※※※※※※※※※※※※※※※※※※※※※",
		""
    };

    @Override
    public void init(ServletConfig config) throws ServletException {
    	for (String cr : copyright) {
    		System.out.println(cr);
		}
        clientClassPath = config.getInitParameter("handle");
        String portText = config.getInitParameter("port");
        if(null != portText){
            port = CommonUtils.stringToInt(portText, port);
        }
        if (clientClassPath == null || clientClassPath.length() < 3) {
            logger.error("Servlet [handle] value as invalid.");
        } else {
            try {
                IMessageHandler handle = (IMessageHandler) Class.forName(clientClassPath).newInstance();
                if (null != handle) {
                	callInit(handle);
                	
                    YSWebSocketPoint.setHandle(handle);
                    
                    TcpServerSocket.getInstance().startServer(port, handle);
                    logger.info("Yisin Communication Init Finished..");
                } else {
                    logger.error("实例化接口【IMessageHandler】对象失败，类【" + clientClassPath + "】必须实现 com.yisin.commun.server.IMessageHandler接口，请检查.");
                }
            } catch (InstantiationException e) {
                logger.error("实例化接口【IMessageHandler】对象失败，类【" + clientClassPath + "】必须实现 com.yisin.commun.server.IMessageHandler接口，请检查.");
            } catch (IllegalAccessException e) {
                logger.error(e.getMessage());
            } catch (ClassNotFoundException e) {
                logger.error("Not found Class " + clientClassPath);
            }
        }

        String compatible = CommonUtils.excNullToString(config.getInitParameter("compatible"), "false");
        if (compatible.equalsIgnoreCase("true")) {
            new TcpPolicyThread().start();
        }
    }
    
    private void callInit(IMessageHandler handle){
    	try {
    		Method method = handle.getClass().getMethod("init", new Class[]{ HandleHelper.class });
    		method.invoke(handle, new Object[]{ HandleHelper.getInstance() });
		} catch (Exception e) {
		}
    }

    private class _YSCommunicationServlet {
        
    }
}
