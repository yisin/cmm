package com.yisin.commun.server;

import com.yisin.commun.modal.Client;
import com.yisin.commun.modal.DataCache;
import com.yisin.commun.modal.DataPackage;

public class BaseMessageHandle implements IMessageHandler {

	/**
	 * 服务初始化时执行的函数
	 */
	@Override
	public void init(HandleHelper helper) {
		
	}
	
	/**
	 * 过滤器，返回True则通过，否则反之
	 */
	@Override
	public boolean filter(Client client, DataPackage dataPackage) {
		return true;
	}
	
	/**
	 * 接收到客户端消息
	 */
    @Override
    public void onMessage(Client client, DataPackage dataPackage) {
        
    }

    /**
     * 客户端关闭了连接
     */
    @Override
    public void onClose(Client client) {
        DataCache.remove(client);
    }

    /**
     * 客户端连接异常断开
     */
    @Override
    public void onError(Client client, Throwable e) {
        DataCache.remove(client);
    }

    /**
     * 客户端发起连接
     */
    @Override
    public void onOpen(Client client) {
        DataCache.add(client.getSid(), client);
    }
    
    public void Send(Client client, DataPackage data){
    	try {
            client.send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private class _BaseMessageHandle {
        
    }
}
