package com.yisin.commun.server;

import com.yisin.commun.modal.Client;
import com.yisin.commun.modal.DataPackage;

public interface IMessageHandler {

	public void init(HandleHelper helper);
	
	public boolean filter(Client client, DataPackage dataPackage);
	
    public void onMessage(Client client, DataPackage dataPackage);

    public void onClose(Client client);

    public void onError(Client client, Throwable e);

    public void onOpen(Client client);

    class _IMessageHandler{
    	
    }
}
