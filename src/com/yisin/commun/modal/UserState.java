package com.yisin.commun.modal;

public class UserState {
	/**在线*/
    public static String Online = "online";
    /**离线*/
    public static String Offline = "offline";
    /**隐身*/
    public static String Stealth = "stealth";
    /**离开*/
    public static String Leave = "leave";
    /**勿扰*/
    public static String AaronLi = "aaronli"; 
}
