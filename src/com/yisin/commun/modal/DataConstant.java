package com.yisin.commun.modal;

public class DataConstant {
	/** 请求类型：消息 */
	public static final String REQUEST_MESSAGE = "msg";
	/** 请求类型：登录 */
	public static final String REQUEST_LOGIN = "login";
	/** 请求类型：退出 */
	public static final String REQUEST_EXIT = "exit";
	/** 请求类型：文件传输 */
	public static final String REQUEST_FILE = "file";
	/** 请求类型：语音 */
	public static final String REQUEST_VOICE = "voice";
	/** 请求类型：视频 */
	public static final String REQUEST_VIDEO = "video";
	
	/** 状态码：正确 */
	public static final int SUCCESS = 1000;
	/** 状态码：错误 */
	public static final int FAILED = 1100;
	/** 状态码：异常 */
	public static final int ERROR = 1900;
	/** 状态码：非法请求 */
	public static final int ILLEGAL = 1003;
	
	private class _DataConstant {
		
	}
	
}
