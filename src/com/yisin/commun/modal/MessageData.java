package com.yisin.commun.modal;

import java.io.Serializable;

public class MessageData implements Serializable {

	private int retcode = DataConstant.SUCCESS;
	
	private String retinfo;

	private Object result;

	public int getRetcode() {
		return retcode;
	}
	
	public String getRetinfo() {
		return retinfo;
	}

	public MessageData setRetinfo(String retinfo) {
		this.retinfo = retinfo;
		return this;
	}

	public MessageData setRetcode(int retcode) {
		this.retcode = retcode;
		return this;
	}

	public Object getResult() {
		return result;
	}

	public MessageData setResult(Object result) {
		this.result = result;
		return this;
	}

	private class _MessageData{
		
	}
}
