package com.yisin.commun.modal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import com.yinsin.other.LogHelper;

public class DataCache {
    private static final LogHelper logger = LogHelper.getLogger(DataCache.class);
    
    private static Hashtable<String, Client> clientTable = new Hashtable<String, Client>();

    public static void add(String sid, Client client) {
        if (clientTable.containsKey(sid)) {
            Client c = clientTable.get(sid);
            if(!c.getUuid().equals(client.getUuid())){
                c.close();
            }
            clientTable.remove(sid);
        }
        clientTable.put(sid, client);
    }

    public static Object removeBySidOrUuid(String sid, String uuid) {
        try {
            Client client = null;
            if (uuid != null) {
                Client cli = clientTable.get(sid);
                if (null != cli && cli.getUuid().equals(uuid)) {
                    client = cli;
                }
            } else if(sid != null){
                client = clientTable.get(sid);
            }
            if(null != client){
                client.close();
                return clientTable.remove(client);
            }
        } catch (Exception e) {
            logger.error("Remove Client[" + sid + "] error：" + e.getMessage());
        }
        return null;
    }

    public static void remove(Client client) {
        if(null != client){
            client.close();
            clientTable.remove(client.getSid());
        }
    }

    public static void removeBySid(String sid) {
        try {
            if (sid != null) {
                Client client = clientTable.get(sid);
                if(null != client){
                    client.close();
                    clientTable.remove(sid);
                }
            }
        } catch (Exception e) {
            logger.error("Remove Client[" + sid + "] error：" + e.getMessage());
        }
    }

    public static Collection<Client> getClients() {
        return clientTable.values();
    }

    public static Client getBySid(String sid) {
        return clientTable.get(sid);
    }

    public static Client getByUuid(String uuid) {
        Iterator<String> ito = clientTable.keySet().iterator();
        Client client = null;
        while (ito.hasNext()) {
            client = clientTable.get(ito.next());
            if (null != client && client.getUuid().equals(uuid)) {
                return client;
            }
        }
        return null;
    }

    /**
     * 根据sid 与 uid 精确获取client对象，两者需同时满足
     * @param sid
     * @param uid
     * @return Client
     */
    public static Client getBySidAndUid(String sid, String uuid) {
        Client client = clientTable.get(sid);
        if (null != client && client.getUuid().equals(uuid)) {
            return client;
        }
        return null;
    }

    /**
     * 根据attr1属性获取client
     * @param attr1
     * @return List<Client>
     */
    public static List<Client> getByAttr1(String attr1) {
        List<Client> result = new ArrayList<Client>();
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c && c.getAttr1() != null && c.getAttr1().equals(attr1)) {
                result.add(c);
            }
        }
        return result;
    }

    /**
     * 根据attr2属性获取client
     * @param attr2
     * @return List<Client>
     */
    public static List<Client> getByAttr2(int attr2) {
        List<Client> result = new ArrayList<Client>();
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c && c.getAttr2() == attr2) {
                result.add(c);
            }
        }
        return result;
    }
    
    /**
     * 根据attr2属性获取client
     * @param attr2
     * @return List<Client>
     */
    public static List<Client> getByKeyValue(String key, Object value) {
        List<Client> result = new ArrayList<Client>();
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c) {
            	if(value instanceof String && c.get(key).toString().equals(value.toString())){
            		result.add(c);
            	} else if(value instanceof Integer && c.get(key) == value){
            		result.add(c);
            	}
            }
        }
        return result;
    }
    
    /**
     * 根据groupId获取client
     * @param groupId
     * @return List<Client>
     */
    public static List<Client> getByGroupId(String groupId) {
        List<Client> result = new ArrayList<Client>();
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c && null != c.getGroupId() && c.getGroupId().equals(groupId)) {
                result.add(c);
            }
        }
        return result;
    }
    
    /**
     * 根据superGroupId获取client
     * @param superGroupId
     * @return List<Client>
     */
    public static List<Client> getBySuperGroupId(String superGroupId) {
        List<Client> result = new ArrayList<Client>();
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c && null != c.getSuperGroupId() && c.getSuperGroupId().equals(superGroupId)) {
                result.add(c);
            }
        }
        return result;
    }
    
    /**
     * 根据groupId获取client，并获取下属所有子对象
     * @param groupId
     * @return List<Client>
     */
    public static List<Client> getAllChildByGroupId(String groupId) {
        List<Client> result = new ArrayList<Client>();
        List<Client> tempList = null;
        Iterator<String> ito = clientTable.keySet().iterator();
        Client c = null;
        while (ito.hasNext()) {
            c = clientTable.get(ito.next());
            if (null != c) {
                if(null != c.getGroupId() && c.getGroupId().equals(groupId)){
                    result.add(c);
                } else if(null != c.getSuperGroupId() && c.getSuperGroupId().equals(groupId)){
                    result.add(c);
                    // 
                    tempList = getAllChildByGroupId(c.getGroupId());
                    if(null != tempList && tempList.size() > 0){
                        result.addAll(tempList);
                    }
                }
            }
        }
        return result;
    }

    private class _DataCache {
        
    }
}
