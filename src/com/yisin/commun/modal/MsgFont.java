package com.yisin.commun.modal;

import java.io.Serializable;

public class MsgFont implements Serializable {
	private String name = "微软雅黑";
	private String color = "000000";
	private int[] style = new int[] {0, 0, 0};
	private int size = 9;

	public String getName() {
		return name;
	}

	public MsgFont setName(String name) {
		this.name = name;
		return this;
	}

	public String getColor() {
		return color;
	}

	public MsgFont setColor(String color) {
		this.color = color;
		return this;
	}

	public int[] getStyle() {
		return style;
	}

	public MsgFont setStyle(int[] style) {
		this.style = style;
		return this;
	}

	public int getSize() {
		return size;
	}

	public MsgFont setSize(int size) {
		this.size = size;
		return this;
	}

	private class _MsgFont{
		
	}
}
