package com.yisin.commun.modal;

import java.io.Serializable;

public class Message implements Serializable {

	private String msg;

	private MsgFont font = new MsgFont();

	public String getMsg() {
		return msg;
	}

	public Message setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public MsgFont getFont() {
		return font;
	}

	public Message setFont(MsgFont font) {
		this.font = font;
		return this;
	}

	private class _Message {
		
	}
	
}
