package com.yisin.commun.modal;

import java.io.Serializable;

public class MessageByteData implements Serializable {

	private int retcode;

	private MessageByteBody result;

	public int getRetcode() {
		return retcode;
	}

	public MessageByteData setRetcode(int retcode) {
		this.retcode = retcode;
		return this;
	}

	public MessageByteBody getResult() {
		return result;
	}

	public MessageByteData setResult(MessageByteBody result) {
		this.result = result;
		return this;
	}
	
	private class _MessageByteData{
		
	}
}
