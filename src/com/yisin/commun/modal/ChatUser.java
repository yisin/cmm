package com.yisin.commun.modal;

import java.io.Serializable;

public class ChatUser implements Serializable, Cloneable {
	private String code;
	private String account;
	private String password;
	private String name;
	private String groupCode;
	private String groupName;
	private String headImage;
	private String position;
	private String state = UserState.Offline;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAccount() {
		return account;
	}

	public ChatUser setAccount(String account) {
		this.account = account;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public ChatUser setPassword(String password) {
		this.password = password;
		return this;
	}

	public String getName() {
		return name;
	}

	public ChatUser setName(String name) {
		this.name = name;
		return this;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public ChatUser setGroupCode(String groupCode) {
		this.groupCode = groupCode;
		return this;
	}

	public String getGroupName() {
		return groupName;
	}

	public ChatUser setGroupName(String groupName) {
		this.groupName = groupName;
		return this;
	}
	
	public String getHeadImage() {
		return headImage;
	}

	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public ChatUser clone() {
		ChatUser user = null;
		try {
			user = (ChatUser) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return user;
	}

	private class _ChatUser {
		
	}
	
}
