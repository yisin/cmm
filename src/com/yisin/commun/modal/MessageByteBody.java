package com.yisin.commun.modal;

import java.io.Serializable;

public class MessageByteBody implements Serializable {

	private ChatUser user;

	private int length;
	private int start;
	private int end;
	private byte[] content;

	public ChatUser getUser() {
		return user;
	}

	public MessageByteBody setUser(ChatUser user) {
		this.user = user;
		return this;
	}

	public int getLength() {
		return length;
	}

	public MessageByteBody setLength(int length) {
		this.length = length;
		return this;
	}

	public int getStart() {
		return start;
	}

	public MessageByteBody setStart(int start) {
		this.start = start;
		return this;
	}

	public int getEnd() {
		return end;
	}

	public MessageByteBody setEnd(int end) {
		this.end = end;
		return this;
	}

	public byte[] getContent() {
		return content;
	}

	public MessageByteBody setContent(byte[] content) {
		this.content = content;
		return this;
	}
	
	private class _MessageByteBody{
		
	}

}
