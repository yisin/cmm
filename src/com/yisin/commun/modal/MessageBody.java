package com.yisin.commun.modal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.yinsin.utils.DateUtils;

public class MessageBody implements Serializable  {

	private ChatUser user;

	private List<Message> content;
	
	private String time = DateUtils.format(new Date(), "YYYY-MM-dd HH:mm:ss");

	public ChatUser getUser() {
		return user;
	}

	public MessageBody setUser(ChatUser user) {
		this.user = user;
		return this;
	}

	public List<Message> getContent() {
		return content;
	}

	public MessageBody setContent(List<Message> content) {
		this.content = content;
		return this;
	}
	
	public MessageBody setContent(Message content) {
		if(null == this.content){
			this.content = new ArrayList<Message>();
		}
		this.content.add(content);
		return this;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	private class _MessageBody{
		
	}
}
