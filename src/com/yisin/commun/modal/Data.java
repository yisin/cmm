package com.yisin.commun.modal;

import java.io.Serializable;

public class Data implements Serializable {

	private String req = DataConstant.REQUEST_MESSAGE;
	private Object value;
	private DataType type = DataType.TEXT;

	public Data() {

	}
	
	public Data(DataType type, String req, Object content) {
		this.type = type;
		this.req = req;
		this.value = content;
	}

	public Data(String req, String content) {
		this.req = req;
		this.value = content;
	}
	
	public Data(DataType type, String req, String content) {
		this.type = type;
		this.req = req;
		this.value = content;
	}

	public Data(String req, MessageData data) {
		this.req = req;
		this.value = data;
	}
	
	public Data(String req, MessageByteData data) {
		this.type = DataType.BYTE;
		this.req = req;
		this.value = data;
	}

	public DataType getType() {
		return type;
	}

	public Data setType(DataType type) {
		this.type = type;
		return this;
	}

	public String getReq() {
		return req;
	}

	public Data setReq(String req) {
		this.req = req;
		return this;
	}

	public Object getValue() {
		return value;
	}

	public Data setValue(Object data) {
		this.value = data;
		return this;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(this.getClass().getName())
		  .append("[HashCode = ").append(this.hashCode())
		  .append(", req = ").append(this.getReq())
		  .append(", type = ").append(this.getType())
		  .append(", value = ").append(this.getValue())
		  .append("]");
		return sb.toString();
	}
	
	private class _Data {
		
	}
}
