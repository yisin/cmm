package com.yisin.commun.exception;

public class AnnotationException extends Exception {
	
	public AnnotationException(){
		super();
	}
	
	public AnnotationException(String message){
		super(message);
	}
	
	public AnnotationException(String message, Throwable thr){
		super(message, thr);
	}
	
	private class _AnnotationException{
		
	}
	
}
