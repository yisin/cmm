# cmm - YsCommun

**YsCommun 介绍：**
----------------

YsCommun是集WebSocket、Socket(TCP/IP)为一体的通讯组件，WebSocket是基于Tomcat7.0.52及以上版本实现的，兼容IE8及以下版本（低版本下使用flash来替代实现），也就是说理论上兼容市面上所有主流浏览器；本组件还集成了Socket通讯，WebSocket与Socket可直接通讯，不需要任何转换操作，只要按照规定的协议组织报文即可，可方便实现网页端、手机端与PC端客户端程序即时通讯功能。

YsCommun 使用方法：
--------------

 - 1、后台
 -- a)	下载jar包
 --- i.	依赖包含3个jar包：fastjson-1.2.31.jar、YsCommun-1.0.jar、YsDevKit-1.12.jar
 -- b)	实现消息处理
 --- i.	自定义消息处理类（例:com.yisin.testyc.YsMessageHandle），继承com.yisin.commun.server.BaseMessageHandle类
   重写BaseMessageHandle类中的相应方法来处理自己的业务逻辑
   方法有：init、onMessage、onOpen、onClose、onError、filter
 --- ii.	消息处理可注册多个处理类，来方便处理不同的业务逻辑：

 

 -- c)	web.xml中配置YsCommun

```xml
<servlet>
		<servlet-name>WebSocketServer</servlet-name>
		<servlet-class>com.yisin.commun.server.YSCommunicationServlet</servlet-class>
		<init-param>
			<param-name>handle</param-name>
			<param-value>com.yisin.testyc.YSMessageHandle</param-value>
		</init-param>
		<init-param>
			<description>此端口号是为了兼容Socket（TCP/IP）协议通讯的，默认为10000</description>
			<param-name>port</param-name>
			<param-value>8090</param-value>
		</init-param>
		<init-param>
			<description>是否开启兼容模式（兼容不支持WebSocket的浏览器，true兼容、false不兼容，默认不兼容）</description>
			<param-name>compatible</param-name>
			<param-value>true</param-value>
		</init-param>
		<load-on-startup>1</load-on-startup>
	</servlet>
```
----

 - 2、WEB端
 -- a、引入脚本文件

```html
<script type="text/javascript" src="yswebsocket-1.0.js"></script>
```
 -- b、编写代码
 
```javascript
var socket = new YSWebSocket({
	url: 'ws://[ip]:[port]/[path]/ws',
	success: function(){
		// TODO
	}, 
	error: function(){
		// TODO
	},
	close: function(){
		// TODO
	},
	message: function(msg){
		// TODO
		var data = JSON.parse(msg);
		if(data.url == 'chat.group'){
			// 群聊
		} else {
			// 其他
		}
	}
});

//发送消息
function sendMsg(url, data){
	//socket.send('user.login', {account: "test", password: "123456"});
	socket.send(url, data);
}
```


 - 3、其他程序，例如c#端使用TCP/IP协议，与cmm的web端进行通讯
 c#程序源代码见[附件][1]，用于测试
![c#开发的简易客户端，使用TCP/IP协议][2]
![c#开发的简易客户端，使用TCP/IP协议][3]


  [1]: https://gitee.com/yisin/cmm/attach_files
  [2]: http://120.27.53.77:8180/screenshot/yscommon.jpg
  [3]: http://120.27.53.77:8180/screenshot/yscommon.gif